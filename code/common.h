#pragma once
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#ifdef _UNICODE
  #define _memtchr wmemchr
#else
  #define _memtchr memchr
#endif

static void report_error(
    LPCTSTR user_message,
    DWORD exit_code,
    BOOL print_error_message)
{
    DWORD error = GetLastError();
    _ftprintf(stderr, _T("%s\n"), user_message);
    if (print_error_message)
    {
        LPTSTR system_message = NULL;
        DWORD message_length = FormatMessage(
            FORMAT_MESSAGE_ALLOCATE_BUFFER |
            FORMAT_MESSAGE_FROM_SYSTEM,
            NULL, error,
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            system_message, 0, NULL);
        if (message_length > 0)
            _ftprintf(stderr, _T("%s\n"), system_message);
        else
            _ftprintf(stderr, _T("Last Error Number; %d.\n"), error);
        if (system_message)
            LocalFree(system_message);
    }

    if (exit_code > 0)
        ExitProcess(exit_code);
}

static DWORD get_options(
    int argc, LPCTSTR argv[],
    LPCTSTR opt_str, ...)
{
    va_list flag_list;
    va_start(flag_list, opt_str);
    LPBOOL flag;
    int flag_index = 0;
    while ((flag = va_arg(flag_list, LPBOOL)) != NULL &&
        (flag_index < (int)_tcslen(opt_str)))
    {
        *flag = false;
        for (int i = 1;
            !(*flag) && (i < argc) && (argv[i][0] == _T('-'));
            ++i)
        {
            *flag = (_memtchr(argv[i], opt_str[flag_index], _tcslen(argv[i])) != NULL);
        }
        ++flag_index;
    }
    va_end(flag_list);

    int arg_index = 1;
    while ((arg_index < argc) && (argv[arg_index][0] == _T('-')))
        ++arg_index;

    return arg_index;
}