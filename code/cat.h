#pragma once
#include "common.h"

static void cat(HANDLE in, HANDLE out)
{
#define buf_size 0x200
    DWORD bytes_read, bytes_written;
    BYTE buf[buf_size];
    while (
        ReadFile(in, buf, buf_size, &bytes_read, NULL) &&
        (bytes_read != 0) &&
        WriteFile(out, buf, bytes_read, &bytes_written, NULL));
#undef buf_size
}

static int cat_std(int argc, LPCTSTR argv[])
{
    HANDLE std_in = GetStdHandle(STD_INPUT_HANDLE);
    HANDLE std_out = GetStdHandle(STD_OUTPUT_HANDLE);

    BOOL dash_s;
    int first_file_index = get_options(argc, argv, _T("s"), &dash_s, NULL);
    if (first_file_index == argc)
    {
        cat(std_in, std_out);
        return 0;
    }

    for (int i = first_file_index; i < argc; ++i)
    {
        HANDLE in = CreateFile(argv[i], GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (in == INVALID_HANDLE_VALUE)
        {
            if (!dash_s)
                report_error(_T("Error: File does not exist."), 0, TRUE);
        }
        else
        {
            cat(in, std_out);
            if (GetLastError() != 0 && !dash_s)
                report_error(_T("Cat Error."), 0, TRUE);
            CloseHandle(in);
        }
    }

    return 0;
}
