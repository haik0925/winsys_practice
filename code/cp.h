#pragma once
#include <stdio.h>
#include <windows.h>

static int cp_c(const char* src, const char* dst)
{
    FILE* in = fopen(src, "rb");
    if (in == NULL)
    {
        perror(src);
        return 2;
    }

    FILE* out = fopen(dst, "wb");
    if (out == NULL)
    {
        perror(dst);
        return 3;
    }

    size_t bytes_in;
    const int buf_size = 256;
    char buf[buf_size];
    while ((bytes_in = fread(buf, 1, buf_size, in)) > 0)
    {
        size_t bytes_out = fwrite(buf, 1, bytes_in, out);
        if (bytes_out != bytes_in)
        {
            perror("Fetal write error");
            return 4;
        }
    }

    fclose(in);
    fclose(out);

    return 0;
}

static int cp_w(const char* src, const char* dst)
{
    HANDLE in = CreateFile(src,
        GENERIC_READ,
        FILE_SHARE_READ, NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL, NULL);
    if (in == INVALID_HANDLE_VALUE)
    {
        printf("Cannot open input file. Error: %x\n", GetLastError());
        return 2;
    }

    HANDLE out = CreateFile(dst,
        GENERIC_WRITE,
        0, NULL,
        CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL, NULL);
    if (out == INVALID_HANDLE_VALUE)
    {
        printf("Cannot open output file. Error: %x\n", GetLastError());
        return 3;
    }

    const int buf_size = 256;
    CHAR buf[buf_size];
    DWORD bytes_read;
    while (ReadFile(in, buf, buf_size, &bytes_read, NULL) && bytes_read > 0)
    {
        DWORD bytes_written;
        WriteFile(out, buf, bytes_read, &bytes_written, NULL);
        if (bytes_written != bytes_read)
        {
            printf("Fetal write error: %x\n", GetLastError());
            return 4;
        }
    }

    CloseHandle(in);
    CloseHandle(out);

    return 0;
}

static int cp_cf(const char* src, const char* dst)
{
    if (!CopyFile(src, dst, FALSE))
    {
        printf("CopyFile Error: %x\n", GetLastError());
        return 2;
    }

    return 0;
}